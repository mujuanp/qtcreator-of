addons_ofxxmlsettings_pri {
    message("MSA/ofxXMLSettings is already included")
} else {

    message("Including MSA/ofxXMLSettings addon into project scope")

    ofxXmlSettingsPath = $$OF_ROOT/addons/ofxXmlSettings

    SOURCES += \
        $$ofxXmlSettingsPath/libs/tinyxml.cpp \
        $$ofxXmlSettingsPath/libs/tinyxmlerror.cpp \
        $$ofxXmlSettingsPath/libs/tinyxmlparser.cpp \
        $$ofxXmlSettingsPath/src/ofxXmlSettings.cpp

    HEADERS += \
        $$ofxXmlSettingsPath/libs/tinyxml.h \
        $$ofxXmlSettingsPath/src/ofxXmlSettings.h

    INCLUDEPATH += \
        $$ofxXmlSettingsPath/src \
        $$ofxXmlSettingsPath/libs

    CONFIG += addons_ofxxmlsettings_pri
}
